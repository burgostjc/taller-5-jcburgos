from django.shortcuts import render, redirect
from django.contrib import auth

def login(request):
	template_name = 'auth_login/login.html'
	data = {}

	auth.logout(request) #hace un logout automaticamente si existe un previo login

	if request.POST:
		username = request.POST['username']
		password = request.POST['password']

		user = auth.authenticate(
			username=username,
			password=password
			)

		if user is not None:
			if user.is_active:
				auth.login(request, user)
				return redirect('basket:list')
			else:
				print("error user not active")
		else:
			print("error user is None")

	return render(request, template_name, data)

def logout(request):
	auth.logout(request)
	return redirect('auth:login')