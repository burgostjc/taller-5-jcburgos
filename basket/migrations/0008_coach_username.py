# Generated by Django 3.0.6 on 2020-06-03 05:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0007_coach_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='coach',
            name='username',
            field=models.CharField(blank=True, max_length=80, null=True, unique=True),
        ),
    ]
