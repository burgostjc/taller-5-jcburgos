from django.urls import path
from basket import views

app_name = 'basket'

urlpatterns = [
	path('', views.list , name='list'),
	path('teams', views.team_list , name='team_list'),
	path('players', views.player_list , name='player_list'),
	path('coach', views.coach_list , name='coach_list'),
	path('match', views.match_list , name='match_list'),
	path('create_team', views.create_team , name='create_team'),
	path('update_team/<int:team_id>', views.update_team , name='update_team'),
	path('delete_team/<int:team_id>', views.delete_team , name='delete_team'),
	path('create_player', views.create_player , name='create_player'),
	path('update_player/<int:player_id>', views.update_player , name='update_player'),
	path('delete_player/<int:player_id>', views.delete_player , name='delete_player'),
	path('create_coach', views.create_coach , name='create_coach'),
	path('update_coach/<int:coach_id>', views.update_coach , name='update_coach'),
	path('delete_coach/<int:coach_id>', views.delete_coach , name='delete_coach'),
	path('update_coach_password', views.update_coach_password , name='update_coach_password'),
]
