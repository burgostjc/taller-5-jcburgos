from django import forms
from .models import Team, Player, Coach
from django.contrib.auth.models import User

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = '__all__'

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = '__all__'

class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = '__all__'

class UserUpdateForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username']