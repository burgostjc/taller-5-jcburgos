from django.shortcuts import render, redirect
from basket.models import Coach, Team, Player, Roster, Match
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from basket.forms import TeamForm, PlayerForm, CoachForm, UserUpdateForm
from django.db.models import Q
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

@login_required #se requiere un login para ver esta pagina
#listar los últimos 5 partidos planificados y los 5 últimos jugadores incorporados a algún equipo. Además debe mostrar la cantidad de jugadores incritos por cada equipo.
def list(request):
	data = {}
	template_name = 'basket/home.html'
	data['last_matches'] = Match.objects.order_by('id').filter(Q(team1__team__coach__user=request.user) | Q(team2__team__coach__user=request.user)).reverse()[:3]
	data['last_matches_superuser'] = Match.objects.order_by('id').reverse()[:3]
	data['last_players'] = Player.objects.order_by('id').filter(team__coach__user=request.user).reverse()[:3]
	data['last_players_superuser'] = Player.objects.order_by('id').all().reverse()[:3]
	data['last_coaches'] = Coach.objects.order_by('id').reverse()[:3]

	return render(request, template_name, data)


@login_required
def team_list(request): #listar todos los equipos registrados en una tabla con paginación.
	data = {}
	template_name = 'basket/teams.html'

	page = request.GET.get('page', 1)
	team_lists = Team.objects.filter(coach__user=request.user)
	paginator = Paginator(team_lists, 2)

	try:
		data['teams'] = paginator.page(page)
	except PageNotAnInteger:
		data['teams'] = paginator.page(1)
	except EmptyPage:
		data['teams'] = paginator.page(paginator.num_pages)


	return render(request, template_name, data)

@login_required
def player_list(request): #listar todos los jugadores agrupados por equipo. Los jugadores deben ser listados a través de una tabla.
	data = {}
	template_name = 'basket/players.html'
	data['teams'] = Team.objects.filter(coach__user=request.user)

	return render(request, template_name, data)

@login_required
def coach_list(request): #listar una tabla con todos los entrenadores registrados e indicar a que equipo pertenece.
	data = {}
	template_name = 'basket/coach.html'
	data["coachs"] = Coach.objects.filter(user=request.user)

	return render(request, template_name, data)

@login_required
def match_list(request): #listar (no necesariamente en una tabla). los partidos registrados. Debe indicar que equipo se enfrenta contra quien y cuando.
	data = {}
	template_name = 'basket/match.html'
	data['matches'] = Match.objects.filter(Q(team1__team__coach__user=request.user) | Q(team2__team__coach__user=request.user))

	return render(request, template_name, data)



@permission_required('basket.add_team')
def create_team(request):
	data = {}
	template_name = 'basket/create_team.html'

	if request.method == 'POST':
		data['form'] = TeamForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save() #si el formulario es valido lo guardo
			return redirect('basket:team_list')
	else:
		data['form'] = TeamForm()

	return render(request, template_name, data)

@permission_required('basket.change_team')
def update_team(request, team_id):
	data = {}
	template_name = 'basket/update_team.html'
	team = Team.objects.get(pk=team_id)

	if request.method == 'POST':
		data['form'] = TeamForm(request.POST,request.FILES or None, instance=team)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('basket:team_list')
	else:
		data['form'] = TeamForm(instance=team)

	return render(request, template_name, data)

@permission_required('basket.delete_team')
def delete_team(request, team_id):
	data = {}
	template_name = 'basket/delete_team.html'
	data['team'] = Team.objects.get(pk=team_id)

	if request.method == 'POST':
		data['team'].delete()
		return redirect('basket:team_list')

	return render(request, template_name, data)


@permission_required('basket.add_player')
def create_player(request):
	data = {}
	template_name = 'basket/create_player.html'

	if request.method == 'POST':
		data['form'] = PlayerForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('basket:player_list')
	else:
		data['form'] = PlayerForm()

	return render(request, template_name, data)

@permission_required('basket.change_player')
def update_player(request, player_id):
	data = {}
	template_name = 'basket/update_player.html'
	player = Player.objects.get(pk=player_id)

	if request.method == 'POST':
		data['form'] = PlayerForm(request.POST,files=request.FILES, instance=player)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('basket:player_list')
	else:
		data['form'] = PlayerForm(instance=player)

	return render(request, template_name, data)

@permission_required('basket.delete_player')
def delete_player(request, player_id):
	data = {}
	template_name = 'basket/delete_player.html'
	data['player'] = Player.objects.get(pk=player_id)

	if request.method == 'POST':
		data['player'].delete()
		return redirect('basket:player_list')

	return render(request, template_name, data)


@permission_required('basket.add_coach')
def create_coach(request):
	data = {}
	template_name = 'basket/create_coach.html'

	if request.method == 'POST':
		data['form'] = CoachForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('basket:coach_list')
	else:
		data['form'] = CoachForm()

	return render(request, template_name, data)

@permission_required('basket.change_coach')
def update_coach(request, coach_id):
	data = {}
	template_name = 'basket/update_coach.html'
	coach = Coach.objects.get(pk=coach_id)

	if request.method == 'POST':
		data['form'] = CoachForm(request.POST,files=request.FILES, instance=coach)
		data['userform'] = UserUpdateForm(request.POST, instance=request.user)
		if data['form'].is_valid() and data['userform'].is_valid():
			data['form'].save()
			data['userform'].save()
			return redirect('basket:coach_list')
	else:
		data['form'] = CoachForm(instance=coach)
		data['userform'] = UserUpdateForm(instance=request.user)

	return render(request, template_name, data)

@permission_required('basket.change_coach')
def update_coach_password(request):
	data = {}
	template_name = 'basket/update_coach_password.html'
	#data['coach'] = Coach.objects.get(pk=coach_id)

	if request.method == 'POST':
		data['form'] = PasswordChangeForm(data=request.POST, user=request.user)
		if data['form'].is_valid():
			data['form'].save()
			update_session_auth_hash(request, data['form'].user)
			return redirect('basket:coach_list')
	else:
		data['form'] = PasswordChangeForm(user=request.user)

	return render(request, template_name, data)

@permission_required('basket.delete_coach')
def delete_coach(request, coach_id):
	data = {}
	template_name = 'basket/delete_coach.html'
	data['coach'] = Coach.objects.get(pk=coach_id)

	if request.method == 'POST':
		data['coach'].delete()
		return redirect('basket:coach_list')

	return render(request, template_name, data)