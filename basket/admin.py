from django.contrib import admin
from django.utils.safestring import mark_safe

from basket.models import (
    Coach,
    Team,
    Player,
    Roster,
    Match
)


@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    search_fields = ['name', ]
    list_display = (
        'name', 
        'description', 
        'code', 
        'render_logo', 
        'coach', 
    )

    def render_logo(self, team):
        return mark_safe("<img src='/media/{}'  width='80' height='80' />".format(team.logo))


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'team_code')
    list_filter = ('team', 'birthday')
    search_fields = ['name', 'nickname', 'rut']
    
    date_hierarchy = 'birthday'

    def team_code(self, player):
        return player.team.code

@admin.register(Roster)
class RosterAdmin(admin.ModelAdmin):
    pass

@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    pass
