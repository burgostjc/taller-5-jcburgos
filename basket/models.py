from django.db import models
from django.contrib.auth.models import User


"""
Nombre entrenador
Edad
Email
Rut
Apodo
"""
class Coach(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    rut = models.CharField(max_length=12)
    nickname = models.CharField(max_length=80)

    def __str__(self):
        return self.name

"""
Nombre del team
Descripción del team
Logo del team
Código de equipo
"""
class Team(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    code = models.CharField(max_length=20, unique=True)
    logo = models.ImageField(upload_to='teams/', blank=True, null=True)
    coach = models.OneToOneField(Coach, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

"""
Nombre jugador
Apodo
Fecha de nacimiento
Edad
Rut
Email
Estatura
Peso
Fotografía
Posición de juego (opciones: Base, Escolta, Alero, Ala-pivot, Pivot)
"""

POSITION_PLAYER = (
    ('BA', 'Base'),
    ('ES', 'Escolta'),
    ('AL', 'Alero'),
    ('AP', 'Ala-pivot'),
    ('PI', 'Pivot'),
)

class Player(models.Model):
    name = models.CharField(max_length=150)
    nickname = models.CharField(max_length=80)
    birthday = models.DateField()
    age = models.PositiveIntegerField()
    rut = models.CharField(max_length=12)
    email = models.EmailField()
    height = models.PositiveIntegerField(help_text='in centimeters')
    weight = models.PositiveIntegerField(help_text='in grams')
    photo = models.ImageField(upload_to='players/', blank=True, null=True)
    position = models.CharField(choices=POSITION_PLAYER, max_length=2)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return '{name} ({team})' . format(name=self.name, team=self.team)



class Roster(models.Model):
    name = models.CharField(max_length=150)
    team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.CASCADE)
    player = models.ManyToManyField(Player)

    def __str__(self):
        return self.name


class Match(models.Model):
    name = models.CharField(max_length=150)
    team1 = models.ForeignKey(Roster, related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Roster, related_name='team2', on_delete=models.CASCADE)
    date = models.DateTimeField()

    def __str__(self):
        return '{name} ({team1} vs {team2})'.format(
            name=self.name,
            team1=self.team1,
            team2=self.team2
        ) 
